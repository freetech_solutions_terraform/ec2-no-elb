#IAM ROLE
resource "aws_iam_instance_profile" "iam_profile" {
  name = "${var.tags["prefix"]}-${var.tags["role"]}-${replace(uuid(), "-", "")}"
  role = module.iam_role.role_id

  lifecycle {
    ignore_changes = [name]
  }
}

#LAUNCH TEMPLATE
resource "aws_launch_template" "launch_template" {
  name_prefix                 = "${var.tags["prefix"]}-${var.tags["role"]}-"
  image_id                    = var.launch_config_image_id
  instance_type               = var.launch_config_instance_type
  iam_instance_profile {
    name = aws_iam_instance_profile.iam_profile.name
  }
  key_name                    = var.launch_config_key_name
  user_data                   = "${base64encode(var.additional_user_data)}"
  network_interfaces {
    associate_public_ip_address = var.launch_config_associate_public_ip_address
    security_groups             = [var.security_group_id]
    delete_on_termination       = true

  }
  monitoring {
    enabled = true
  }
  block_device_mappings {
    device_name = var.launch_config_root_block_device_volume_name
    ebs {
      volume_size = var.launch_config_root_block_device_volume_size
      volume_type = var.launch_config_root_block_device_volume_type
      delete_on_termination = true
    }
  }
  ebs_optimized = var.launch_config_ebs_optimized

}

resource "aws_autoscaling_group" "autoscaling_group" {
  name                      = "${var.tags["prefix"]}-${var.tags["role"]}"
  min_size                  = var.asg_min_size
  max_size                  = var.asg_max_size
  desired_capacity          = var.asg_desired_capacity
  vpc_zone_identifier       = var.asg_ec2_subnet_ids
  #launch_configuration      = aws_launch_configuration.launch_config.name
  health_check_grace_period = var.asg_health_check_grace_period
  health_check_type         = var.asg_health_check_type

  target_group_arns = var.asg_target_group_arns

  #depends_on = [aws_launch_configuration.launch_config]
  launch_template {
    id      = aws_launch_template.launch_template.id
    version = "$Latest"
  }
  depends_on = [aws_launch_template.launch_template]

  lifecycle {
    ignore_changes = [
      name,
    ]
    create_before_destroy = true
  }

  dynamic "tag" {
    for_each = var.asg_tag_names
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}

resource "aws_autoscaling_schedule" "scale_in" {
  count                  = var.asg_schedule_scale_in_recurrence != "" ? 1 : 0
  scheduled_action_name  = "${var.tags["prefix"]}-${var.tags["role"]}-scheduled-scale-in"
  min_size               = var.asg_schedule_scale_in_min_size
  max_size               = var.asg_schedule_scale_in_max_size
  desired_capacity       = var.asg_schedule_scale_in_desired_capacity
  recurrence             = var.asg_schedule_scale_in_recurrence
  autoscaling_group_name = aws_autoscaling_group.autoscaling_group.name
}

resource "aws_autoscaling_schedule" "scale_out" {
  count                  = var.asg_schedule_scale_out_recurrence != "" ? 1 : 0
  scheduled_action_name  = "${var.tags["prefix"]}-${var.tags["role"]}-scheduled-scale-out"
  min_size               = var.asg_schedule_scale_out_min_size
  max_size               = var.asg_schedule_scale_out_max_size
  desired_capacity       = var.asg_schedule_scale_out_desired_capacity
  recurrence             = var.asg_schedule_scale_out_recurrence
  autoscaling_group_name = aws_autoscaling_group.autoscaling_group.name
}
