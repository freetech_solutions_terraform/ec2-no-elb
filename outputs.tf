// The name of the autoscale group
output "autoscaling_group_name" {
  value = aws_autoscaling_group.autoscaling_group.name
}

// The Amazon Resource Name (ARN) specifying the role
output "iam_role_id" {
  value = module.iam_role.role_id
}

// The name of the the role
output "iam_role_name" {
  value = module.iam_role.role_name
}
